package com.serg.videosample;

import android.Manifest;
import android.app.Application;

import com.serg.videosample.di.comp.DaggerMainComponent;
import com.serg.videosample.di.comp.MainComponent;
import com.serg.videosample.di.module.MainModule;
import com.serg.videosample.model.CameraDecoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CameraDecoderApplication extends Application {

    private CameraDecoder cameraDecoder;
    private static MainComponent component;
    private List<String> permissions;

    @Override
    public void onCreate() {
        super.onCreate();
        permissions = Arrays.asList(Manifest.permission.CAMERA,
                Manifest.permission.RECORD_AUDIO,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
        cameraDecoder = new CameraDecoder();
        component = DaggerMainComponent.builder()
                .mainModule(new MainModule(this))
                .build();
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public CameraDecoder getCameraDecoder() {
        return cameraDecoder;
    }

    public static MainComponent getComponent() {
        return component;
    }
}
