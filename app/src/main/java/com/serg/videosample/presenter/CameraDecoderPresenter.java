package com.serg.videosample.presenter;

import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.View;

import com.serg.videosample.CameraDecoderApplication;
import com.serg.videosample.R;
import com.serg.videosample.model.CameraDecoder;
import com.serg.videosample.view.ICameraView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import javax.inject.Inject;

public class CameraDecoderPresenter implements ICameraDecoderPresenter {

    private ICameraView cameraView;
    @Inject
    CameraDecoder cameraDecoder;

    public CameraDecoderPresenter(ICameraView cameraView) {
        this.cameraView = cameraView;
        cameraDecoder = CameraDecoderApplication.getComponent().getCameraDecoder();
    }

    public void startRecording() {
        setFabStopRecordAction();
        if (!cameraDecoder.isRecorderInited())
            prepareView4Recorder();
        cameraDecoder.startRecorder();
    }

    public void stopRecording() {
        setFabStartRecordAction();
        cameraDecoder.stopRecorder();
        resetRecording();
        cameraDecoder.decodeVideo();
        prepareView4Recorder();
    }

    public void resetRecording() {
        if (cameraDecoder.isRecorderInited())
            cameraDecoder.resetRecorder();
    }

    public void initCameraView(SurfaceHolder.Callback callback, View.OnClickListener clickListener) {
        cameraView.getHolder().addCallback(callback);
        cameraView.getHolder().setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
        cameraView.getFab().setOnClickListener(clickListener);
    }

    public void prepareView4Recorder() {
        cameraDecoder.initRecorder();
        Log.d("Recorder", "setPreview->prepare");
        cameraDecoder.getRecorder().setPreviewDisplay(cameraView.getHolder().getSurface());
        try {
            cameraDecoder.getRecorder().prepare();
        } catch (IllegalStateException e) {
            e.printStackTrace();
            //finish();
        } catch (IOException e) {
            e.printStackTrace();
            //finish();
        }
    }

    public void startCameraPreview(SurfaceHolder holder){
        if (!cameraDecoder.isCamOpened()) {
            cameraDecoder.openCamera();
            if (cameraDecoder.getCamera()!= null) {
                Camera.Parameters parameters = cameraDecoder.getCamera().getParameters();
                cameraDecoder.getCamera().setParameters(parameters);
                try {
                    cameraDecoder.getCamera().setPreviewDisplay(holder);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                cameraDecoder.getCamera().startPreview();
            }
            cameraDecoder.setCamOpened(true);
        }
    }

    public void stopCameraPreview() {
        if (cameraDecoder.getCamera() != null){
            cameraDecoder.getCamera().release();
            cameraDecoder.setCamOpened(false);
        }
    }

    @Override
    public void update(Observable observable, Object o) {
        if (null != o && o instanceof String) {
            Log.d("Decoder", "dec state in presenter: " + cameraDecoder.isDecoding());
            if (cameraDecoder.isDecoding())
                setFabEncodeProcessing();
            else
                setFabStartRecordAction();
        }
    }

    private void setFabStartRecordAction() {
        cameraView.getProgressBar().setVisibility(View.GONE);
        cameraView.getFab().setImageResource(R.drawable.circle);
        cameraView.getFab().setClickable(true);
    }

    private void setFabStopRecordAction() {
        cameraView.getProgressBar().setVisibility(View.GONE);
        cameraView.getFab().setImageResource(R.drawable.stop);
        cameraView.getFab().setClickable(true);
    }

    private void setFabEncodeProcessing() {
        cameraView.getProgressBar().setVisibility(View.VISIBLE);
        cameraView.getFab().setImageResource(R.drawable.movie);
        cameraView.getFab().setClickable(false);
    }
}