package com.serg.videosample.presenter;

import android.view.SurfaceHolder;
import android.view.View;

import java.util.Observer;

public interface ICameraDecoderPresenter extends Observer {
    void initCameraView(SurfaceHolder.Callback callback, View.OnClickListener clickListener);
    void prepareView4Recorder();
    void startRecording();
    void stopRecording();
    void resetRecording();
    void startCameraPreview(SurfaceHolder holder);
    void stopCameraPreview();
}
