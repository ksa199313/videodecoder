package com.serg.videosample.utils;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import java.util.List;

public class PermissionHelper {

    public static void requestPermissions(Activity activity, List<String> permissions) {
        ActivityCompat.requestPermissions(activity,
                permissions.toArray(new String[permissions.size()]), 1);
    }

    public static boolean checkPermissions(Activity activity, List<String> permissions) {
        boolean result = true;
        for (String permission :
                permissions) {
            result = result && (ContextCompat
                    .checkSelfPermission(
                            activity,
                            permission)
                    == PackageManager.PERMISSION_GRANTED);
        }
        return result;
    }
}
