package com.serg.videosample.model;

import android.hardware.Camera;
import android.media.CamcorderProfile;
import android.media.MediaRecorder;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.SurfaceHolder;

import com.daasuu.mp4compose.composer.Mp4Composer;
import com.daasuu.mp4compose.filter.GlGrayScaleFilter;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Observable;

public class CameraDecoder extends Observable {

    private MediaRecorder recorder;
    private List<String> lastFiles;
    private Camera camera;

    boolean recording;
    boolean decoding;
    boolean recorderInited;
    private boolean camOpened;

    public CameraDecoder() {
        this.recorder = new MediaRecorder();
        this.lastFiles = new ArrayList<>();
        this.recording = false;
        this.decoding = false;
        this.recorderInited = false;
        this.camOpened = false;
    }

    public void openCamera(){
        this.camera = Camera.open();
    }

    public Camera getCamera() {
        return camera;
    }

    public void setCamera(Camera camera) {
        this.camera = camera;
    }

    public boolean isCamOpened() {
        return camOpened;
    }

    public void setCamOpened(boolean camOpened) {
        this.camOpened = camOpened;
    }

    public boolean isRecorderInited() {
        return recorderInited;
    }

    public void setRecorderInited(boolean recorderInited) {
        this.recorderInited = recorderInited;
    }

    public void initRecorder() {
        Log.d("Capture", "INIT recorder");
        Log.d("Recorder", "setAudio->setVideo->setProfile->setOutput->setDur->setFiSize");
        if (!recorderInited) {
            this.recorder = new MediaRecorder();
            recorder.setAudioSource(MediaRecorder.AudioSource.DEFAULT);
            recorder.setVideoSource(MediaRecorder.VideoSource.DEFAULT);

            CamcorderProfile cpHigh = CamcorderProfile
                    .get(CamcorderProfile.QUALITY_HIGH);
            recorder.setProfile(cpHigh);

            lastFiles.add(getVideoFile());
            recorder.setOutputFile(lastFiles.get(lastFiles.size() - 1));
            recorder.setMaxDuration(50000); // 50 seconds
            recorder.setMaxFileSize(50000000); // 50 megabytes

            recorderInited = true;
        }
    }

    public String getVideoFile() {
        File videoDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "VideoList");
        if (!videoDir.exists()) {
            if (!videoDir.mkdirs()) {
                Log.d("Capture", "failed to create dir");
                return null;
            }
        }
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());

        lastFiles.add(videoDir.getPath() + File.separator +
                "REC_" + timeStamp + ".mp4");

        return videoDir.getPath() + File.separator +
                "REC_" + timeStamp + ".mp4";
    }

    public void clearBuffers(){
        File videoDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "VideoList");
        if (!videoDir.exists()) {
            if (!videoDir.mkdirs()) {
                Log.d("Capture", "failed to create dir");
            }
        }
        File[] emptyFiles = videoDir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File file) {
                return file.length() == 0;
            }
        });
        if (emptyFiles != null) {
            for (int i = 0; i < emptyFiles.length; i++) {
                Log.d("Capture", "file: " + emptyFiles[i].getAbsolutePath() + " d: " + emptyFiles[i].delete());
            }
        }
    }

    public boolean isDecoding() {
        return decoding;
    }

    public void resetRecorder() {
        Log.d("Recorder", "resetRec");

        getRecorder().reset();
        setRecorderInited(false);
    }

    public void stopRecorder() {
        Log.d("Recorder", "stopRec");
        getRecorder().stop();
        setRecording(false);
    }

    public void startRecorder() {
        Log.d("Recorder", "startRec");
        getRecorder().start();
        setRecording(true);
    }

    public void setDecoding(boolean decoding) {
        this.decoding = decoding;

        Log.d("Decoder", "dec state: " + decoding);

        setChanged();
        notifyObservers("Decode changed");
    }

    private final Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            setDecoding(false);
        }
    };

    public void decodeVideo() {
        setDecoding(true);
        Log.d("Capture", "Last file: " + lastFiles.get(lastFiles.size()-1));
        new Mp4Composer(lastFiles.get(lastFiles.size()-1),
                lastFiles.get(lastFiles.size()-1).substring(
                        0,
                        lastFiles.get(lastFiles.size()-1).length() - 4)+"Encoded.mp4")
                .size(1280, 720)
                .filter(new GlGrayScaleFilter())
                .listener(new Mp4Composer.Listener() {
                    @Override
                    public void onProgress(double progress) {
                        Log.d("Capture", "progress");
                    }

                    @Override
                    public void onCompleted() {
                        Log.d("Capture", "Completed!");
                        handler.sendEmptyMessage(1);
                    }

                    @Override
                    public void onCanceled() {
                        Log.d("Capture", "Cancelled!");
                        //handler.sendEmptyMessage(2);
                    }

                    @Override
                    public void onFailed(Exception exception) {
                        Log.d("Capture", "Failed!");
                        //handler.sendEmptyMessage(-1);
                    }
                }).start();
    }

    public MediaRecorder getRecorder() {
        return recorder;
    }

    public void setRecorder(MediaRecorder recorder) {
        this.recorder = recorder;
    }

    public List<String> getLastFiles() {
        return lastFiles;
    }

    public void setLastFiles(List<String> lastFiles) {
        this.lastFiles = lastFiles;
    }

    public boolean isRecording() {
        return recording;
    }

    public void setRecording(boolean recording) {
        this.recording = recording;
        setChanged();
        notifyObservers(this);
    }
}
