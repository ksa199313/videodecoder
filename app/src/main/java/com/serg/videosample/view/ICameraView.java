package com.serg.videosample.view;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.view.SurfaceHolder;
import android.widget.ProgressBar;

public interface ICameraView {
    SurfaceHolder getHolder();
    FloatingActionButton getFab();
    CoordinatorLayout getMainLayout();
    ProgressBar getProgressBar();
}
