package com.serg.videosample.view;

import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.view.SurfaceHolder;
import android.widget.ProgressBar;

public class CameraView implements ICameraView {

    private SurfaceHolder holder;
    private FloatingActionButton fab;
    private CoordinatorLayout mainLayout;
    private ProgressBar progressBar;

    public CameraView(SurfaceHolder holder, FloatingActionButton fab, CoordinatorLayout mainLayout, ProgressBar progressBar) {
        this.holder = holder;
        this.fab = fab;
        this.mainLayout = mainLayout;
        this.progressBar = progressBar;
    }

    public ProgressBar getProgressBar() {
        return progressBar;
    }

    public void setProgressBar(ProgressBar progressBar) {
        this.progressBar = progressBar;
    }

    public SurfaceHolder getHolder() {
        return holder;
    }

    public void setHolder(SurfaceHolder holder) {
        this.holder = holder;
    }

    public FloatingActionButton getFab() {
        return fab;
    }

    public void setFab(FloatingActionButton fab) {
        this.fab = fab;
    }

    public CoordinatorLayout getMainLayout() {
        return mainLayout;
    }

    public void setMainLayout(CoordinatorLayout mainLayout) {
        this.mainLayout = mainLayout;
    }
}
