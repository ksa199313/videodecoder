package com.serg.videosample;

import android.Manifest;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.serg.videosample.model.CameraDecoder;
import com.serg.videosample.presenter.CameraDecoderPresenter;
import com.serg.videosample.presenter.ICameraDecoderPresenter;
import com.serg.videosample.utils.PermissionHelper;
import com.serg.videosample.view.CameraView;
import com.serg.videosample.view.ICameraView;

import java.io.IOException;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.util.List;

import javax.inject.Inject;

public class VideoCapture extends AppCompatActivity implements View.OnClickListener, SurfaceHolder.Callback {

    private ICameraView cameraView;
    private ICameraDecoderPresenter presenter;
    @Inject CameraDecoder cameraDecoder;
    @Inject List<String> permissions;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.d("CamRec", "OnCreate");

        cameraDecoder = CameraDecoderApplication.getComponent().getCameraDecoder();
        permissions = CameraDecoderApplication.getComponent().getPermissions();

        setContentView(R.layout.activity_main);
        cameraView = new CameraView(
                ((SurfaceView) findViewById(R.id.CameraView)).getHolder(),
                (FloatingActionButton) findViewById(R.id.fab),
                (CoordinatorLayout) findViewById(R.id.main_layout),
                (ProgressBar) findViewById(R.id.decoding_progress)
        );
        presenter = new CameraDecoderPresenter(
                cameraView
        );
        presenter.initCameraView(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d("CamRec", "OnResume");
        if (!PermissionHelper.checkPermissions(this, permissions)) {
            cameraDecoder.setCamOpened(false);
            PermissionHelper.requestPermissions(this, permissions);
        }
        cameraDecoder.addObserver(presenter);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("CamRec", "OnPause");
        cameraDecoder.deleteObserver(presenter);
        presenter.resetRecording();
        presenter.stopCameraPreview();
        cameraDecoder.clearBuffers();
    }

    @Override
    public void onClick(View v) {
        if (cameraDecoder.isRecording()) {
            presenter.stopRecording();
            presenter.startCameraPreview(cameraView.getHolder());
        } else {
            presenter.stopCameraPreview();
            presenter.startRecording();
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (PermissionHelper.checkPermissions(this, permissions))
            presenter.startCameraPreview(holder);
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width,
                               int height) {
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (cameraDecoder.isRecording()) {
            cameraDecoder.stopRecorder();
        }
        presenter.resetRecording();
        presenter.stopCameraPreview();
        finish();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                grantResults[2] == PackageManager.PERMISSION_GRANTED) {
            cameraDecoder.setCamOpened(false);
            presenter.startCameraPreview(cameraView.getHolder());
        }
    }
}