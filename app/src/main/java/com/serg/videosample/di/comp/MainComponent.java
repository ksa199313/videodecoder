package com.serg.videosample.di.comp;

import com.serg.videosample.di.module.MainModule;
import com.serg.videosample.model.CameraDecoder;

import java.util.List;

import dagger.Component;

@Component(
        modules = {
                MainModule.class
        }
)
public interface MainComponent {
    CameraDecoder getCameraDecoder();
    List<String> getPermissions();
}
