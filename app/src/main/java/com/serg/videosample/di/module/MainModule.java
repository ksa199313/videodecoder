package com.serg.videosample.di.module;

import com.serg.videosample.CameraDecoderApplication;
import com.serg.videosample.model.CameraDecoder;

import java.util.List;

import dagger.Module;
import dagger.Provides;

@Module
public class MainModule {
    private CameraDecoderApplication app;

    public MainModule(CameraDecoderApplication app) {
        this.app = app;
    }

    @Provides
    CameraDecoderApplication provideApp(){
        return app;
    }

    @Provides
    CameraDecoder provideCameraDecoder(CameraDecoderApplication app){
        return app.getCameraDecoder();
    }

    @Provides
    List<String> providePermissions(CameraDecoderApplication app){
        return app.getPermissions();
    }
}
